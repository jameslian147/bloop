/*
 *  ****************************************************************************
 *  Copyright (c) 2015, MasterCard International Incorporated and/or its
 *  affiliates. All rights reserved.
 *  <p/>
 *  The contents of this file may only be used subject to the MasterCard
 *  Mobile Payment SDK for MCBP and/or MasterCard Mobile MPP UI SDK
 *  Materials License.
 *  <p/>
 *  Please refer to the file LICENSE.TXT for full details.
 *  <p/>
 *  TO THE EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 *  WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NON INFRINGEMENT. TO THE EXTENT PERMITTED BY LAW, IN NO EVENT SHALL
 *  MASTERCARD OR ITS AFFILIATES BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 *  *****************************************************************************
 */

package com.mastercard.foreverythingelse.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mastercard.foreverythingelse.DataManager;
import com.mastercard.foreverythingelse.R;
import com.mastercard.foreverythingelse.adapter.BasketAdapter;
import com.mastercard.foreverythingelse.helpers.DialogHelper;
import com.mastercard.foreverythingelse.helpers.StringHelper;
import com.mastercard.foreverythingelse.manager.ShippingAddressesManager;
import com.mastercard.foreverythingelse.mco.PaymentConfirmationIntegration;
import com.mastercard.foreverythingelse.model.Basket;
import com.mastercard.foreverythingelse.model.BasketItem;
import com.mastercard.foreverythingelse.widget.FontButton;
import com.mastercard.masterpass.core.MasterPassException;
import com.mastercard.masterpass.merchant.AmountData;
import com.mastercard.masterpass.merchant.AuthorizationResponse;
import com.mastercard.masterpass.merchant.MasterPass;
import com.mastercard.masterpass.merchant.MasterPassButton;
import com.mastercard.masterpass.merchant.TransactionDetails;
import com.mastercard.masterpass.merchant.TransactionResultListener;

import java.util.Currency;

/***
 * Activity used at checkout.
 */
public class PayOnDeliveryActivity extends BaseActivity {

    /**
     * TAG used for logging.
     */
    private static final String TAG = PayOnDeliveryActivity.class.getName();

    /**
     * Delay for "Completing payment" dialog
     */
    private static final int DIALOG_DELAY_MILLIS = 1500;

    /**
     * UI Elements.
     */
    private BasketAdapter mBasketAdapter;
    private TextView mTxtSubtotal;

    /**
     * The "Buy with MasterPass" button.
     */
    private RelativeLayout mLayoutBuyWithMasterPass;

    /**
     * Defines if the progress dialog should close after a delay, or if it should close
     * immediately when returning from MCO.
     */
    private boolean mDelayCloseProgressDialog = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_on_delivery);

        // Configure the Action Bar
        mLayoutBuyWithMasterPass = (RelativeLayout) findViewById(R.id.layout_buy_with_masterpass);

        // Ensure no shipping is currently applied

        Toolbar toolbar = findViewById(R.id.my_toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.drawable.flipkart_text_icon);
        getSupportActionBar().setTitle("");

        // Ensure the scroll view is scrolled right to the top
        final ScrollView mScrollView = (ScrollView) findViewById(R.id.scrollview);
        mScrollView.post(new Runnable() {
            @Override
            public void run() {
                mScrollView.scrollTo(0, 0);
            }
        });

        // Setup the list of products
//        mBasketAdapter = new BasketAdapter(this, mBasket, this);
        String[] items = {"1 x Blue Dress", "1 x Blue Cap", "1 x Pair of Blue Shoes", "1 x Blue Jacket", "1 x Blue Backpack"};

        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
        ListView mListProducts = (ListView) findViewById(android.R.id.list);
        mListProducts.setAdapter(itemsAdapter);
//        mListProducts.setOnItemClickListener(this);


        //Button checkoutButton = (Button) findViewById(R.id.btn_checkout);
        Button checkoutButton = (Button) findViewById(R.id.btn_checkout);
        checkoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogHelper.showProgressDialog(PayOnDeliveryActivity.this, "Completing Payment");

                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        // Close last instantiated progress dialog
                        DialogHelper.closeProgressDialog();
                        startActivity(new Intent(PayOnDeliveryActivity.this, CompletePayOnDeliveryActivity.class));
                    }
                }, DIALOG_DELAY_MILLIS);


            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        updateBadge(DataManager.getInstance().mBasket.mTotalItems);
        if (mDelayCloseProgressDialog) {

            mDelayCloseProgressDialog = false;

            Log.v(TAG, "Delayed close dialog");

            // After a short delay, close the dialog that was opened in transactionInitiated()
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {

                    // Close last instantiated progress dialog
                    DialogHelper.closeProgressDialog();
                }
            }, DIALOG_DELAY_MILLIS);

        } else {
            // In case user clicked 'Back' and returned to this page
            Log.v(TAG, "Immediate close dialog");

            // Close last instantiated progress dialog
            DialogHelper.closeProgressDialog();

        }

    }

    /**
     * Override the onBackPressed behaviour to force navigation to the products page.
     */
    @Override
    public void onBackPressed() {
        backToProducts();
    }

    /**
     * Navigate to the products page whilst clearing the backstack.
     */
    private void backToProducts() {
        Intent intent = new Intent(this, ProductsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
    }
}