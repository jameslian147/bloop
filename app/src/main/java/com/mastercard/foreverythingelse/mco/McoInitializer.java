/*
 *  ****************************************************************************
 *  Copyright (c) 2015, MasterCard International Incorporated and/or its
 *  affiliates. All rights reserved.
 *  <p/>
 *  The contents of this file may only be used subject to the MasterCard
 *  Mobile Payment SDK for MCBP and/or MasterCard Mobile MPP UI SDK
 *  Materials License.
 *  <p/>
 *  Please refer to the file LICENSE.TXT for full details.
 *  <p/>
 *  TO THE EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 *  WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NON INFRINGEMENT. TO THE EXTENT PERMITTED BY LAW, IN NO EVENT SHALL
 *  MASTERCARD OR ITS AFFILIATES BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 *  *****************************************************************************
 */

package com.mastercard.foreverythingelse.mco;

import android.content.Context;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.mastercard.foreverythingelse.BuildConfig;
import com.mastercard.foreverythingelse.Constants;
import com.mastercard.foreverythingelse.DataManager;
import com.mastercard.foreverythingelse.network.ResponseListener;
import com.mastercard.foreverythingelse.network.RestMethod;
import com.mastercard.foreverythingelse.network.api.ApiList;
import com.mastercard.foreverythingelse.util.PreferencesHelper;
import com.mastercard.masterpass.core.EnvironmentType;
import com.mastercard.masterpass.core.MasterPassException;
import com.mastercard.masterpass.core.SessionKeySigningRequest;
import com.mastercard.masterpass.core.SessionKeySigningResponse;
import com.mastercard.masterpass.mc.core.crypto.CryptoUtil;
import com.mastercard.masterpass.mc.merchant.mpswitch.MockLocalMasterPassSwitchServices;
import com.mastercard.masterpass.merchant.InitializationListener;
import com.mastercard.masterpass.merchant.MasterPass;
import com.mastercard.masterpass.merchant.MasterPassMerchantConfig;

import java.util.Locale;

/**
 * Used to initialize the [MCO-SDK] library.
 */
public class McoInitializer {
    /**
     * Logging TAG
     */
    private static final String TAG = McoInitializer.class.getName();

    /**
     * Methods that create config object and initialize MCO-SDK library
     *
     * @param applicationContext app context
     * @throws MasterPassException
     */
    public void initMCO(final Context applicationContext) throws MasterPassException {


        Log.d(TAG, "initLiveMCO");
        // Create MasterPass Merchant Config
        //final MasterPassMerchantConfig masterPassMerchantConfig = Config.getMasterPassWalletConfig(applicationContext);


        final MasterPassMerchantConfig masterPassMerchantConfig = new MasterPassMerchantConfig(
                Locale.US,// Only US is supported in this version of MCO
                Constants.SUPPORTED_PAYMENT_NETWORKS,
                PreferencesHelper.INSTANCE.getCryptogramTypeEnum(applicationContext),
                false,
                EnvironmentType.SANDBOX);

        // For live use real servers
        // Create public key by simple Base64 encoding
        String publicKey = Base64.encodeToString(CryptoUtil.getSessionKeyPair().getPublic().getEncoded(), Base64.DEFAULT);

        // Call Merchant Server to get Session Key Signing (Signature used for further communication)
        DataManager.getInstance().getNetworkManager().makeApiCall(RestMethod.POST,
                ApiList.Method.API_SESSION_KEY_SIGNING,
                new com.mastercard.foreverythingelse.network.api.request.SessionKeySigningRequest("b08902f1-4e80-433c-b66f-61a71d646379", BuildConfig.VERSION_NAME, publicKey),
                new ResponseListener<com.mastercard.foreverythingelse.network.api.response.SessionKeySigningResponse>() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                        DataManager.getInstance().setMcoInitialized(false);
                        Toast.makeText(applicationContext, "Failed to contact the Test Merchant Server. Please restart the app.", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(com.mastercard.foreverythingelse.network.api.response.SessionKeySigningResponse response) {
                        Log.d(TAG, "Signature: " + response.sessionSignature);
                        // Attempt to initialize the MCO SDK.
                        try {
                            MasterPass.getInstance().init(
                                    applicationContext,
                                    masterPassMerchantConfig,
                                    convertResponse(response),
                                    mInitializationListener);
                        } catch (MasterPassException e) {
                            e.printStackTrace();
                        }
                    }
                });

    }

    /*
     * Initialization listener that defines behaviour to occur at certain initialization states.
     */
    private static final InitializationListener mInitializationListener = new InitializationListener() {
        @Override
        public void initializationFailed(MasterPassException e) {
            Log.e(TAG, "Init MCO: initializationFailed");
            e.printStackTrace();
            DataManager.getInstance().setMcoInitialized(false);
        }

        public void paymentMethodAvailable(){

        }

    };



    /**
     * Converts our model to MCO model for SessionKeySigning response model
     *
     * @param response our model
     * @return MCO model
     */
    public static SessionKeySigningResponse convertResponse(com.mastercard.foreverythingelse.network.api.response.SessionKeySigningResponse response) {
        SessionKeySigningResponse result = new SessionKeySigningResponse();
        result.setAppId(response.appId);
        result.setAppVersion(response.appVersion);
        result.setAppSigningPublicKey(response.appSigningPublicKey);
        result.setSessionSignature(response.sessionSignature);
        return result;
    }


}
