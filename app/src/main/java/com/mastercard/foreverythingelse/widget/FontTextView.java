/*
 *  ****************************************************************************
 *  Copyright (c) 2015, MasterCard International Incorporated and/or its
 *  affiliates. All rights reserved.
 *  <p/>
 *  The contents of this file may only be used subject to the MasterCard
 *  Mobile Payment SDK for MCBP and/or MasterCard Mobile MPP UI SDK
 *  Materials License.
 *  <p/>
 *  Please refer to the file LICENSE.TXT for full details.
 *  <p/>
 *  TO THE EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 *  WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NON INFRINGEMENT. TO THE EXTENT PERMITTED BY LAW, IN NO EVENT SHALL
 *  MASTERCARD OR ITS AFFILIATES BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 *  *****************************************************************************
 */
package com.mastercard.foreverythingelse.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.mastercard.foreverythingelse.R;

import java.util.Hashtable;

public class FontTextView extends TextView {
    private static Hashtable<String, Typeface> fonts = new Hashtable<>();

    public FontTextView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);

        if (!isInEditMode()) {
            final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.FontTextView);
            final String font = a.getString(R.styleable.FontTextView_fontname);
            setTypeface(getFont(font));
        }
    }

    public FontTextView(final Context context, final AttributeSet attrs) {
        super(context, attrs);

        if (!isInEditMode()) {
            Typeface defaultFont;
            final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.FontTextView);
            final String font = a.getString(R.styleable.FontTextView_fontname);
            if (font != null) {
                defaultFont = getFont(font);
            } else {
                defaultFont = getFont(context.getResources().getString(R.string.font));
            }

            setTypeface(defaultFont);
        }
    }

    public FontTextView(final Context context) {
        super(context);

        if (!isInEditMode()) {
            setTypeface(getFont(context.getResources().getString(R.string.font)));
        }
    }

    private Typeface getFont(final String fontName) {
        Typeface type = fonts.get(fontName);
        if (type == null) {
            type = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + fontName);
            fonts.put(fontName, type);
        }
        return type;
    }
}