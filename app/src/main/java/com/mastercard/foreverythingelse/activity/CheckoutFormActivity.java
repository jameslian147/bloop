package com.mastercard.foreverythingelse.activity;

import android.os.Bundle;
import android.widget.TextView;

import com.mastercard.foreverythingelse.DataManager;
import com.mastercard.foreverythingelse.R;
import com.mastercard.foreverythingelse.helpers.StringHelper;
import com.mastercard.foreverythingelse.model.Basket;

public class CheckoutFormActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_form);

        configureActionBar();

        TextView txtSubtotal = (TextView) findViewById(R.id.txt_subtotal);

        Basket basket = DataManager.getInstance().mBasket;
        txtSubtotal.setText(StringHelper.asCurrency(basket.mCurrencyCode, basket.mSubTotal));
    }
}
