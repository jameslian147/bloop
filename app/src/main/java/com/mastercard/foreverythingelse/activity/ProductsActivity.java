/*
 *  ****************************************************************************
 *  Copyright (c) 2015, MasterCard International Incorporated and/or its
 *  affiliates. All rights reserved.
 *  <p/>
 *  The contents of this file may only be used subject to the MasterCard
 *  Mobile Payment SDK for MCBP and/or MasterCard Mobile MPP UI SDK
 *  Materials License.
 *  <p/>
 *  Please refer to the file LICENSE.TXT for full details.
 *  <p/>
 *  TO THE EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 *  WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NON INFRINGEMENT. TO THE EXTENT PERMITTED BY LAW, IN NO EVENT SHALL
 *  MASTERCARD OR ITS AFFILIATES BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 *  *****************************************************************************
 */

package com.mastercard.foreverythingelse.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ScrollView;


import com.mastercard.foreverythingelse.Constants;
import com.mastercard.foreverythingelse.DataManager;
import com.mastercard.foreverythingelse.R;
import com.mastercard.foreverythingelse.adapter.ProductListAdapter;
import com.mastercard.foreverythingelse.model.Product;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * Activity with Products list that can be added to the basket.
 */
public class ProductsActivity extends BaseActivity implements AdapterView.OnItemClickListener, ProductListAdapter.AddToCartListener {

    /**
     * TAG used for logging.
     */
    private static final String TAG = ProductsActivity.class.getName();

    /**
     * List of available products
     */
    private final ArrayList<Product> mProducts = new ArrayList<>(Arrays.asList(Constants.PRODUCTS));

    /**
     * UI Elements.
     */
    private ProductListAdapter mProductListAdapter;

    private ScrollView mScrollView;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.products_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        showPayOnDelivery();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setContentView(R.layout.activity_products);
        mScrollView = (ScrollView) findViewById(R.id.scrollview);
        // Configure the Action Bar
//        configureActionBar(true);
        Toolbar toolbar = findViewById(R.id.my_toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setIcon(R.drawable.flipkart_text_icon);

        // Ensure the scroll view is at the top of the page
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mScrollView.scrollTo(0, 0);
            }
        }, 10);

        // Setup the list of products
        mProductListAdapter = new ProductListAdapter(this, mProducts, this);
        GridView listProducts = (GridView) findViewById(R.id.grid);
        listProducts.setAdapter(mProductListAdapter);
        listProducts.setOnItemClickListener(this);

//        ActionBar actionBar = getActionBar();
//        View actionBarView = actionBar.getCustomView();
//        final ImageView notificationView = actionBarView.findViewById(R.id.notification);
//        notificationView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showDialog();
//            }
//        });

       /* //Setup hero product
        final ImageView imgHero = (ImageView) findViewById(R.id.img_hero_product);
        TextView txtHeroLabel = (TextView) findViewById(R.id.txt_hero_label);
        TextView txtHeroPrice = (TextView) findViewById(R.id.txt_hero_price);

        imgHero.setImageResource(Constants.PRODUCTS[0].mLargeImageResource);
        txtHeroLabel.setText(Constants.PRODUCTS[0].mName);
        txtHeroPrice.setText(StringHelper.asCurrency(Constants.CURRENCY_CODE, Constants.PRODUCTS[0].mPrice));


        // Ensure the scroll view is at the top of the page
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mScrollView.scrollTo(0, 0);
            }
        }, 200);

        // Buy now for hero product
        TextView btnBuyNow = (TextView) findViewById(R.id.btn_buy_now);
        btnBuyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Product product = Constants.PRODUCTS[0];
                int numProductsInBasket = DataManager.getInstance().mBasket.addProduct(product);
                updateBadge(numProductsInBasket);

                animateAddToCart(product, imgHero);
            }
        });
        // Ensure the scroll view is at the top of the page
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mScrollView.scrollTo(0, 0);
            }
        }, 10);*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");

        // Ensure the scroll view is at the top of the page
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mScrollView.scrollTo(0, 0);
            }
        }, 10);

        mProductListAdapter.notifyDataSetChanged();

        // When resuming, update the badge with the current total (can be updated on the checkout page)
//        updateBadge(DataManager.getInstance().mBasket.mTotalItems);

        // Ensure the scroll view is at the top of the page
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mScrollView.scrollTo(0, 0);
            }
        }, 500);

    }


    private void showPayOnDelivery() {
        startActivity(new Intent(ProductsActivity.this, PayOnDeliveryActivity.class));
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        goToProductDetail(position);
    }

    @Override
    public void addToCart(Product product, View parentView, ImageView productImageView) {
        int numProductsInBasket = DataManager.getInstance().mBasket.addProduct(product);
        updateBadge(numProductsInBasket);

        animateAddToCart(product, productImageView);
    }
}
