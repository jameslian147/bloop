/*
 *  ****************************************************************************
 *  Copyright (c) 2015, MasterCard International Incorporated and/or its
 *  affiliates. All rights reserved.
 *  <p/>
 *  The contents of this file may only be used subject to the MasterCard
 *  Mobile Payment SDK for MCBP and/or MasterCard Mobile MPP UI SDK
 *  Materials License.
 *  <p/>
 *  Please refer to the file LICENSE.TXT for full details.
 *  <p/>
 *  TO THE EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 *  WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NON INFRINGEMENT. TO THE EXTENT PERMITTED BY LAW, IN NO EVENT SHALL
 *  MASTERCARD OR ITS AFFILIATES BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 *  *****************************************************************************
 */

package com.mastercard.foreverythingelse;

import com.mastercard.foreverythingelse.model.Product;
import com.mastercard.masterpass.core.MasterPassCard;

import java.util.EnumSet;

public class Constants {
    /**
     * List of products to use within the application.
     */
    public static final Product[] PRODUCTS = {
            // Football
            new Product("Soccer Ball",
                    "When the titans of soccer clash in Barcelona, this is the ball at the center of the action. It has a seamless surface for true flight off the foot and features high-end materials in the cover, backing and bladder. It features a 32-panel design and bright, high-contrast graphics for a cool look and easy tracking. The rubber-blend construction and rubber bladder promise durability and responsive action.",
                    4,
                    2300.00,
                    R.drawable.product_balls_orange,
                    R.drawable.product_balls_orange_large,
                    false,
                    false),

            // Gloves
            new Product("Soccer gloves",
                    "Get a grip wearing these goalkeeper gloves, which have latex palms for excellent tact. Their positive cut gives you a greater ball-contact area, while a vented cuff and half-wrap wrist strap allow for a greater range of motion during play.",
                    4.2f,
                    1500.00,
                    R.drawable.product_gloves_orange,
                    R.drawable.product_gloves_orange_large,
                    false,
                    false),

            // Shoes
            new Product("Soccer shoes",
                    "Take on the turf in the new men's low-cut soccer shoes. They're crafted using lightweight synthetic leather for exceptional comfort and great ball feel, while the die-cut liner provides responsive shock absorption on impact. Multidirectional and varied-length rubber studs deliver dynamic traction and aggressive control.",
                    3,
                    3100.00,
                    R.drawable.product_shoes_red,
                    R.drawable.product_shoes_red_large,
                    false,
                    false),

            // Socks
            new Product("Soccer socks",
                    "When you're on the field, you need socks that will help you perform at your best. This 2-pack of soccer socks boasts moisture-wicking technology and features arch and ankle compression for stability. Targeted cushioning on the footbed enhances shock absorption for those unexpected quick kicks.",
                    5,
                    350.00,
                    R.drawable.product_socks_red,
                    R.drawable.product_socks_red_large,
                    false,
                    false),

            // tshirt
            new Product("Soccer tshirt",
                    "This short-sleeve jersey will be your favorite thing to wear! It's perfect for cheering on the team or kicking the ball around himself; the top's sweat-wicking fabric and mesh underarm ventilation help keep his core temp in check as he heats up.",
                    2,
                    3000.00,
                    R.drawable.product_tshirt_red,
                    R.drawable.product_tshirt_red_large,
                    false,
                    true),
            // Socks
            new Product("Soccer socks",
                    "When you're on the field, you need socks that will help you perform at your best. This 2-pack of soccer socks boasts moisture-wicking technology and features arch and ankle compression for stability. Targeted cushioning on the footbed enhances shock absorption for those unexpected quick kicks.",
                    5,
                    350.00,
                    R.drawable.product_socks_red,
                    R.drawable.product_socks_red_large,
                    false,
                    false),
            // Socks
            new Product("Soccer socks",
                    "When you're on the field, you need socks that will help you perform at your best. This 2-pack of soccer socks boasts moisture-wicking technology and features arch and ankle compression for stability. Targeted cushioning on the footbed enhances shock absorption for those unexpected quick kicks.",
                    5,
                    350.00,
                    R.drawable.product_socks_red,
                    R.drawable.product_socks_red_large,
                    false,
                    false),// Socks
            new Product("Soccer socks",
                    "When you're on the field, you need socks that will help you perform at your best. This 2-pack of soccer socks boasts moisture-wicking technology and features arch and ankle compression for stability. Targeted cushioning on the footbed enhances shock absorption for those unexpected quick kicks.",
                    5,
                    350.00,
                    R.drawable.product_socks_red,
                    R.drawable.product_socks_red_large,
                    false,
                    false),
            // Socks
            new Product("Soccer socks",
                    "When you're on the field, you need socks that will help you perform at your best. This 2-pack of soccer socks boasts moisture-wicking technology and features arch and ankle compression for stability. Targeted cushioning on the footbed enhances shock absorption for those unexpected quick kicks.",
                    5,
                    350.00,
                    R.drawable.product_socks_red,
                    R.drawable.product_socks_red_large,
                    false,
                    false),// Socks
            new Product("Soccer socks",
                    "When you're on the field, you need socks that will help you perform at your best. This 2-pack of soccer socks boasts moisture-wicking technology and features arch and ankle compression for stability. Targeted cushioning on the footbed enhances shock absorption for those unexpected quick kicks.",
                    5,
                    350.00,
                    R.drawable.product_socks_red,
                    R.drawable.product_socks_red_large,
                    false,
                    false),// Socks
            new Product("Soccer socks",
                    "When you're on the field, you need socks that will help you perform at your best. This 2-pack of soccer socks boasts moisture-wicking technology and features arch and ankle compression for stability. Targeted cushioning on the footbed enhances shock absorption for those unexpected quick kicks.",
                    5,
                    350.00,
                    R.drawable.product_socks_red,
                    R.drawable.product_socks_red_large,
                    false,
                    false),
            // Socks
            new Product("Soccer socks",
                    "When you're on the field, you need socks that will help you perform at your best. This 2-pack of soccer socks boasts moisture-wicking technology and features arch and ankle compression for stability. Targeted cushioning on the footbed enhances shock absorption for those unexpected quick kicks.",
                    5,
                    350.00,
                    R.drawable.product_socks_red,
                    R.drawable.product_socks_red_large,
                    false,
                    false),
            // Socks
            new Product("Soccer socks",
                    "When you're on the field, you need socks that will help you perform at your best. This 2-pack of soccer socks boasts moisture-wicking technology and features arch and ankle compression for stability. Targeted cushioning on the footbed enhances shock absorption for those unexpected quick kicks.",
                    5,
                    350.00,
                    R.drawable.product_socks_red,
                    R.drawable.product_socks_red_large,
                    false,
                    false),
            // Socks
            new Product("Soccer socks",
                    "When you're on the field, you need socks that will help you perform at your best. This 2-pack of soccer socks boasts moisture-wicking technology and features arch and ankle compression for stability. Targeted cushioning on the footbed enhances shock absorption for those unexpected quick kicks.",
                    5,
                    350.00,
                    R.drawable.product_socks_red,
                    R.drawable.product_socks_red_large,
                    false,
                    false),
            // Socks
            new Product("Soccer socks",
                    "When you're on the field, you need socks that will help you perform at your best. This 2-pack of soccer socks boasts moisture-wicking technology and features arch and ankle compression for stability. Targeted cushioning on the footbed enhances shock absorption for those unexpected quick kicks.",
                    5,
                    350.00,
                    R.drawable.product_socks_red,
                    R.drawable.product_socks_red_large,
                    false,
                    false),
            // Socks
            new Product("Soccer socks",
                    "When you're on the field, you need socks that will help you perform at your best. This 2-pack of soccer socks boasts moisture-wicking technology and features arch and ankle compression for stability. Targeted cushioning on the footbed enhances shock absorption for those unexpected quick kicks.",
                    5,
                    350.00,
                    R.drawable.product_socks_red,
                    R.drawable.product_socks_red_large,
                    false,
                    false),
            // Socks
            new Product("Soccer socks",
                    "When you're on the field, you need socks that will help you perform at your best. This 2-pack of soccer socks boasts moisture-wicking technology and features arch and ankle compression for stability. Targeted cushioning on the footbed enhances shock absorption for those unexpected quick kicks.",
                    5,
                    350.00,
                    R.drawable.product_socks_red,
                    R.drawable.product_socks_red_large,
                    false,
                    false),
            // Socks
            new Product("Soccer socks",
                    "When you're on the field, you need socks that will help you perform at your best. This 2-pack of soccer socks boasts moisture-wicking technology and features arch and ankle compression for stability. Targeted cushioning on the footbed enhances shock absorption for those unexpected quick kicks.",
                    5,
                    350.00,
                    R.drawable.product_socks_red,
                    R.drawable.product_socks_red_large,
                    false,
                    false),
            // Socks
            new Product("Soccer socks",
                    "When you're on the field, you need socks that will help you perform at your best. This 2-pack of soccer socks boasts moisture-wicking technology and features arch and ankle compression for stability. Targeted cushioning on the footbed enhances shock absorption for those unexpected quick kicks.",
                    5,
                    350.00,
                    R.drawable.product_socks_red,
                    R.drawable.product_socks_red_large,
                    false,
                    false),
            // Socks
            new Product("Soccer socks",
                    "When you're on the field, you need socks that will help you perform at your best. This 2-pack of soccer socks boasts moisture-wicking technology and features arch and ankle compression for stability. Targeted cushioning on the footbed enhances shock absorption for those unexpected quick kicks.",
                    5,
                    350.00,
                    R.drawable.product_socks_red,
                    R.drawable.product_socks_red_large,
                    false,
                    false),
            // Socks
            new Product("Soccer socks",
                    "When you're on the field, you need socks that will help you perform at your best. This 2-pack of soccer socks boasts moisture-wicking technology and features arch and ankle compression for stability. Targeted cushioning on the footbed enhances shock absorption for those unexpected quick kicks.",
                    5,
                    350.00,
                    R.drawable.product_socks_red,
                    R.drawable.product_socks_red_large,
                    false,
                    false),
            // Socks
            new Product("Soccer socks",
                    "When you're on the field, you need socks that will help you perform at your best. This 2-pack of soccer socks boasts moisture-wicking technology and features arch and ankle compression for stability. Targeted cushioning on the footbed enhances shock absorption for those unexpected quick kicks.",
                    5,
                    350.00,
                    R.drawable.product_socks_red,
                    R.drawable.product_socks_red_large,
                    false,
                    false),
            // Socks
            new Product("Soccer socks",
                    "When you're on the field, you need socks that will help you perform at your best. This 2-pack of soccer socks boasts moisture-wicking technology and features arch and ankle compression for stability. Targeted cushioning on the footbed enhances shock absorption for those unexpected quick kicks.",
                    5,
                    350.00,
                    R.drawable.product_socks_red,
                    R.drawable.product_socks_red_large,
                    false,
                    false)





    };

    /**
     * Currency code to use throughout the application.
     */
    public static final String CURRENCY_CODE = "INR";

    public static final String DEFAULT_BRAND_ID = "master";

    public static final String DEFAULT_ORIGIN_URL = "http://mastercard.com";


    /**
     * List of supported payments networks by this Merchant
     */
    public static final EnumSet SUPPORTED_PAYMENT_NETWORKS = EnumSet.of(MasterPassCard.PaymentNetwork.AMEX,
            MasterPassCard.PaymentNetwork.DISCOVER,
            MasterPassCard.PaymentNetwork.MASTERCARD,
            MasterPassCard.PaymentNetwork.VISA);
}
