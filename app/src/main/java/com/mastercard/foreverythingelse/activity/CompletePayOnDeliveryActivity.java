/*
 *  ****************************************************************************
 *  Copyright (c) 2015, MasterCard International Incorporated and/or its
 *  affiliates. All rights reserved.
 *  <p/>
 *  The contents of this file may only be used subject to the MasterCard
 *  Mobile Payment SDK for MCBP and/or MasterCard Mobile MPP UI SDK
 *  Materials License.
 *  <p/>
 *  Please refer to the file LICENSE.TXT for full details.
 *  <p/>
 *  TO THE EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 *  WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NON INFRINGEMENT. TO THE EXTENT PERMITTED BY LAW, IN NO EVENT SHALL
 *  MASTERCARD OR ITS AFFILIATES BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 *  *****************************************************************************
 */

package com.mastercard.foreverythingelse.activity;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.mastercard.foreverythingelse.DataManager;
import com.mastercard.foreverythingelse.R;
import com.mastercard.foreverythingelse.adapter.CompleteBasketAdapter;
import com.mastercard.foreverythingelse.helpers.StringHelper;
import com.mastercard.foreverythingelse.model.Basket;

/**
 * Activity representing complete transaction screen.
 * It shows bought products and summary section
 */
public class CompletePayOnDeliveryActivity extends BaseActivity {

    /**
     * The current basket.
     */
    private final Basket mBasket = DataManager.getInstance().mBasket;

    /**
     * UI Elements.
     */
    private TextView mTxtSubtotal;
    private TextView mTxtTotal;
    private TextView mShippingOptionTotal;
    private TextView mDiscount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_on_delivery);

        Toolbar toolbar = findViewById(R.id.my_toolbar);
        // Configure the Action Bar
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.drawable.flipkart_text_icon);
        getSupportActionBar().setTitle("");

        // Ensure the scroll view is scrolled right to the top
        final ScrollView mScrollView = (ScrollView) findViewById(R.id.scrollview);
        mScrollView.post(new Runnable() {
            @Override
            public void run() {
                mScrollView.scrollTo(0, 0);
            }
        });

        // Setup the list of products
        String[] items = {"1 x Blue Dress", "1 x Blue Cap", "1 x Pair of Blue Shoes", "1 x Blue Jacket", "1 x Blue Backpack"};
        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
        ListView mListProducts = (ListView) findViewById(android.R.id.list);
        mListProducts.setAdapter(itemsAdapter);

        // Update the total and subtotal
        mTxtSubtotal = (TextView) findViewById(R.id.txt_subtotal);

        // Bind click event to the continue shopping button
        (findViewById(R.id.btn_continue_shopping)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                returnToProducts(true);
                showDialog();
            }
        });

        // We now need to clear the basket for the next time round
        DataManager.getInstance().clearBasket();
        updateBadge(DataManager.getInstance().mBasket.mTotalItems);
    }

    @Override
    public void onBackPressed() {
        // Return to Products
        returnToProducts(false);
    }

    private void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(CompletePayOnDeliveryActivity.this);
        builder.setTitle("Which Courier you want to notify?")
                .setItems(R.array.merchant_array, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        notifyApp();
                    }
                });
        builder.create().show();
    }


    public void notifyApp() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "channel";
            String description = "description";
            String channelId = "CHANNEL_ID";

            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(channelId, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, channelId)
                    .setSmallIcon(R.drawable.icon_mc)
                    .setContentTitle("Payment Notification")
                    .setContentText("Payment Piece")
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);

            notificationManager.notify(1, mBuilder.build());
            Toast.makeText(this, "Thank you, Muthu has been notified", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Proceed to Products page
     *
     * @param isSlideFromRight indicate from where the new page should be animated
     */
    private void returnToProducts(boolean isSlideFromRight) {
        Intent intent = new Intent(CompletePayOnDeliveryActivity.this, ProductsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
        if (isSlideFromRight) {
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
        } else {
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        }
    }

    /**
     * Update the subtotal and total based on the current basket.
     */
    private void updateBasketTotals() {
        mTxtSubtotal.setText(StringHelper.asCurrency(mBasket.mCurrencyCode, mBasket.mSubTotal));
        mTxtTotal.setText(StringHelper.asCurrency(mBasket.mCurrencyCode, mBasket.getTotal()));
        mShippingOptionTotal.setText(StringHelper.asCurrency(mBasket.mCurrencyCode, mBasket.mShippingPrice));
        mDiscount.setText("-" + StringHelper.asCurrency(mBasket.mCurrencyCode, mBasket.getDiscount()));
    }
}
