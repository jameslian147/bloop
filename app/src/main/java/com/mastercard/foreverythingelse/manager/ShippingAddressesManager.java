/*
 *  ****************************************************************************
 *  Copyright (c) 2015, MasterCard International Incorporated and/or its
 *  affiliates. All rights reserved.
 *  <p/>
 *  The contents of this file may only be used subject to the MasterCard
 *  Mobile Payment SDK for MCBP and/or MasterCard Mobile MPP UI SDK
 *  Materials License.
 *  <p/>
 *  Please refer to the file LICENSE.TXT for full details.
 *  <p/>
 *  TO THE EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 *  WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NON INFRINGEMENT. TO THE EXTENT PERMITTED BY LAW, IN NO EVENT SHALL
 *  MASTERCARD OR ITS AFFILIATES BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 *  *****************************************************************************
 */

package com.mastercard.foreverythingelse.manager;

import android.util.Log;

import com.activeandroid.query.Select;
import com.mastercard.foreverythingelse.model.DbAddressModel;
import com.mastercard.masterpass.core.MasterPassAddress;
import com.mastercard.masterpass.core.MasterPassException;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Shipping addresses management helper.
 * It performs various db operations
 */
public enum ShippingAddressesManager {
    INSTANCE; // singleton

    /**
     * TAG used for logging.
     */
    private static final String TAG = ShippingAddressesManager.class.getName();

    private MasterPassAddress mAddressUsedForTransaction;

    ShippingAddressesManager() {
    }

    public MasterPassAddress getDefaultShippingAddress() {

        DbAddressModel dbAddressModel = new Select()
                .from(DbAddressModel.class)
                .where("IsDefault = ?", true)
                .executeSingle();

        return dbModelToMasterPassAddress(dbAddressModel);
    }

    public DbAddressModel getAddressById(long id) {

        return new Select()
                .from(DbAddressModel.class)
                .where("Id = ?", id)
                .executeSingle();
    }

    public List<DbAddressModel> getAddresses() {

        return new Select()
                .from(DbAddressModel.class)
                .execute();
    }

    public List<MasterPassAddress> dbAddressListToMasterPassAddressList(List<DbAddressModel> dbAddressModelList) {

        List<MasterPassAddress> addressList = new ArrayList<>();
        for (DbAddressModel dbAddressModel : dbAddressModelList) {
            MasterPassAddress masterPassAddress = dbModelToMasterPassAddress(dbAddressModel);
            if (masterPassAddress != null) {
                addressList.add(masterPassAddress);
            }
        }
        return addressList;
    }


    public DbAddressModel updateAddress(DbAddressModel dbAddressModel) {
        dbAddressModel.save();
        return dbAddressModel;
    }

    public void deleteAddress(long id) {

        DbAddressModel dbAddressModel = DbAddressModel.load(DbAddressModel.class, id);
        dbAddressModel.delete();
    }

    public DbAddressModel insertAddress(DbAddressModel dbAddressModel) {

        dbAddressModel.save();
        return dbAddressModel;
    }

    private MasterPassAddress dbModelToMasterPassAddress(DbAddressModel dbAddressModel) {
        if (dbAddressModel == null) {
            return null;
        }

        try {
            return new MasterPassAddress.Builder(Locale.US)
                    .firstName(dbAddressModel.firstName)
                    .lastName(dbAddressModel.lastName)
                    .addressLine(0, dbAddressModel.addressLine1)
                    .addressLine(1, dbAddressModel.addressLine2)
                    .locality(dbAddressModel.city)
                    .adminArea(dbAddressModel.adminArea)
                    .postalCode(dbAddressModel.postcode)
                    .build();
        } catch (MasterPassException ex) {
            Log.e(TAG, "Unable to build MasterPassAddress object from database entry (DbAddressModel). "
                    + ex.getMessage());
            return null;
        }
    }

    public DbAddressModel setDefaultShippingAddress(boolean isDefault, DbAddressModel defaultAddress) {

        List<DbAddressModel> dbAddressModelList = getAddresses();
        for (DbAddressModel dbAddressModel : dbAddressModelList) {
            dbAddressModel.isDefault = false;
            dbAddressModel.save();
        }
        defaultAddress.isDefault = isDefault;
        defaultAddress.save();
        return defaultAddress;
    }

    public void setAddressUsedForTransaction(MasterPassAddress addressUsedForTransaction) {
        this.mAddressUsedForTransaction = addressUsedForTransaction;
    }

    public void saveAddressUsedForTransaction() {
        if (mAddressUsedForTransaction != null) {
            // Check if address already exists
            if (!checkIfExists(mAddressUsedForTransaction)) {
                DbAddressModel dbAddressModel = new DbAddressModel();
                dbAddressModel.firstName = mAddressUsedForTransaction.getFirstName();
                dbAddressModel.lastName = mAddressUsedForTransaction.getLastName();
                dbAddressModel.addressLine1 = mAddressUsedForTransaction.getAddressLine(0);
                dbAddressModel.addressLine2 = mAddressUsedForTransaction.getAddressLine(1);
                dbAddressModel.postcode = mAddressUsedForTransaction.getPostalCode();
                dbAddressModel.city = mAddressUsedForTransaction.getLocality();
                dbAddressModel.adminArea = mAddressUsedForTransaction.getAdminArea();
                dbAddressModel.alias = dbAddressModel.addressLine1;
                dbAddressModel.isDefault = false;
                this.insertAddress(dbAddressModel);
            }
        }
    }

    /**
     * Iterate over addresses convert to MasterPassAddress and compare Id
     * @param address MasterPassAddress
     * @return true if address already exists based on MasterPassAddress.getId()
     */
    private boolean checkIfExists(MasterPassAddress address) {
        for (DbAddressModel dbAddress : this.getAddresses()) {
            if (dbModelToMasterPassAddress(dbAddress).getId().equalsIgnoreCase(address.getId())) {
                return true;
            }
        }
        return false;
    }
}
