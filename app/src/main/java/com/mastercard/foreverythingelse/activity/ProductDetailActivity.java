package com.mastercard.foreverythingelse.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mastercard.foreverythingelse.Constants;
import com.mastercard.foreverythingelse.DataManager;
import com.mastercard.foreverythingelse.R;
import com.mastercard.foreverythingelse.adapter.ProductListAdapter;
import com.mastercard.foreverythingelse.helpers.StringHelper;
import com.mastercard.foreverythingelse.model.Product;

import java.util.ArrayList;
import java.util.Arrays;

public class ProductDetailActivity extends BaseActivity implements AdapterView.OnItemClickListener {
    public static final String INTENT_EXTRA_PRODUCT_INDEX = "product_index";

    /**
     * List of available products
     */
    private ArrayList<Product> mProducts = new ArrayList<>(Arrays.asList(Constants.PRODUCTS));

    /**
     * The product we are displaying
     */
    private Product mProduct;

    /**
     * UI Elements.
     */
    private ProductListAdapter mProductListAdapter;
    private ImageView mProductImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        // Configure the action bar
        configureActionBar(true);

        // Find the container
        mFrameContainer = (FrameLayout) findViewById(R.id.container);

        // Find out what product it is (and remove it from the list)
        int position = getIntent().getIntExtra(INTENT_EXTRA_PRODUCT_INDEX, 0);
        mProduct = mProducts.remove(position);

        // We only want to show 4 products so just get the first 4
        mProducts = new ArrayList<>(mProducts.subList(0, 4));

        // Update the UI for this product
        mProductImage = (ImageView) findViewById(R.id.img_image);
        updateUIForProduct();

        // Setup the list of products
        mProductListAdapter = new ProductListAdapter(this, mProducts);
        GridView listProducts = (GridView) findViewById(R.id.grid);
        listProducts.setAdapter(mProductListAdapter);
        listProducts.setOnItemClickListener(this);

        // Ensure the scroll view is at the top of the page
        ScrollView scrollView = (ScrollView) findViewById(R.id.scrollview);
        scrollView.scrollTo(0, 0);

        // Setup add to cart button
        TextView btnAddToCart = (TextView) findViewById(R.id.btn_add_to_cart);
        btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int numProductsInBasket = DataManager.getInstance().mBasket.addProduct(mProduct);
                updateBadge(numProductsInBasket);

                animateAddToCart(mProduct, mProductImage);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        mProductListAdapter.notifyDataSetChanged();

        // When resuming, update the badge with the current total (can be updated on the checkout page)
        updateBadge(DataManager.getInstance().mBasket.mTotalItems);
    }

    /**
     * Update the UI elements for the current product
     */
    private void updateUIForProduct() {
        TextView productName = (TextView) findViewById(R.id.txt_name);
        TextView productPrice = (TextView) findViewById(R.id.txt_price);
        TextView productDescription = (TextView) findViewById(R.id.txt_description);

        mProductImage.setImageResource(mProduct.mLargeImageResource);
        productName.setText(mProduct.mName);
        productPrice.setText(StringHelper.asCurrency(Constants.CURRENCY_CODE, mProduct.mPrice));
        productDescription.setText(mProduct.mDescription);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ArrayList<Product> products = new ArrayList<>(Arrays.asList(Constants.PRODUCTS));

        Product selectedProduct = mProducts.get(position);

        for (int i = 0; i < products.size(); i++) {
            if (selectedProduct.mName.equals(products.get(i).mName)) {
                goToProductDetail(i);
                break;
            }
        }
        finish();
    }

}
