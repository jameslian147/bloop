/*
 *  ****************************************************************************
 *  Copyright (c) 2015, MasterCard International Incorporated and/or its
 *  affiliates. All rights reserved.
 *  <p/>
 *  The contents of this file may only be used subject to the MasterCard
 *  Mobile Payment SDK for MCBP and/or MasterCard Mobile MPP UI SDK
 *  Materials License.
 *  <p/>
 *  Please refer to the file LICENSE.TXT for full details.
 *  <p/>
 *  TO THE EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 *  WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NON INFRINGEMENT. TO THE EXTENT PERMITTED BY LAW, IN NO EVENT SHALL
 *  MASTERCARD OR ITS AFFILIATES BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 *  *****************************************************************************
 */

package com.mastercard.foreverythingelse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mastercard.foreverythingelse.Constants;
import com.mastercard.foreverythingelse.R;
import com.mastercard.foreverythingelse.helpers.StringHelper;
import com.mastercard.foreverythingelse.model.Basket;
import com.mastercard.foreverythingelse.model.BasketItem;

/**
 * Adapter for list of products in current basket.
 * Used on CheckoutActivity and on CompleteActivity
 */
public class CompleteBasketAdapter extends ArrayAdapter<BasketItem> {

    /**
     * Ctor
     *
     * @param context current context
     * @param basket  current basket
     */
    public CompleteBasketAdapter(Context context, Basket basket) {
        super(context, R.layout.list_item_complete_basket_item, basket.mBasketItems);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CompleteBasketItemHolder holder;

        // If we've not been passed a view (i.e. not recycling) we need to inflate one
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_complete_basket_item, null);

            // Create a new holder and tag it against the view
            holder = new CompleteBasketItemHolder(convertView);
            convertView.setTag(holder);

        } else {

            // Grab the current tag
            holder = (CompleteBasketItemHolder) convertView.getTag();
        }

        // Populate the view with the data from the product in the current position
        holder.populate(getItem(position));

        // Return the populate view
        return convertView;
    }

    /**
     * Holder class to the re-use of views.
     */
    private class CompleteBasketItemHolder {
        // Widgets
        private final ImageView mImage;
        private final TextView mTxtTitle;
        private final TextView mTxtPrice;
        private final TextView mTxtQuantity;

        private CompleteBasketItemHolder(View container) {
            mImage = (ImageView) container.findViewById(R.id.img_image);
            mTxtTitle = (TextView) container.findViewById(R.id.txt_name);
            mTxtPrice = (TextView) container.findViewById(R.id.txt_price);
            mTxtQuantity = (TextView) container.findViewById(R.id.txt_quantity);
        }

        private void populate(final BasketItem item) {
            mImage.setImageResource(item.mProduct.mImageResource);
            mTxtTitle.setText(item.mProduct.mName);
            mTxtPrice.setText(StringHelper.asCurrency(Constants.CURRENCY_CODE, item.mProduct.mPrice * item.mQuantity));
            mTxtQuantity.setText(String.valueOf(item.mQuantity));
        }
    }
}
