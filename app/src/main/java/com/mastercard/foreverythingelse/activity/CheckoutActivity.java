/*
 *  ****************************************************************************
 *  Copyright (c) 2015, MasterCard International Incorporated and/or its
 *  affiliates. All rights reserved.
 *  <p/>
 *  The contents of this file may only be used subject to the MasterCard
 *  Mobile Payment SDK for MCBP and/or MasterCard Mobile MPP UI SDK
 *  Materials License.
 *  <p/>
 *  Please refer to the file LICENSE.TXT for full details.
 *  <p/>
 *  TO THE EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 *  WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NON INFRINGEMENT. TO THE EXTENT PERMITTED BY LAW, IN NO EVENT SHALL
 *  MASTERCARD OR ITS AFFILIATES BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 *  *****************************************************************************
 */

package com.mastercard.foreverythingelse.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mastercard.foreverythingelse.DataManager;
import com.mastercard.foreverythingelse.R;
import com.mastercard.foreverythingelse.adapter.BasketAdapter;
import com.mastercard.foreverythingelse.helpers.DialogHelper;
import com.mastercard.foreverythingelse.helpers.StringHelper;
import com.mastercard.foreverythingelse.manager.ShippingAddressesManager;
import com.mastercard.foreverythingelse.mco.PaymentConfirmationIntegration;
import com.mastercard.foreverythingelse.model.Basket;
import com.mastercard.foreverythingelse.model.BasketItem;
import com.mastercard.masterpass.core.MasterPassException;
import com.mastercard.masterpass.merchant.AmountData;
import com.mastercard.masterpass.merchant.AuthorizationResponse;
import com.mastercard.masterpass.merchant.MasterPass;
import com.mastercard.masterpass.merchant.MasterPassButton;
import com.mastercard.masterpass.merchant.TransactionDetails;
import com.mastercard.masterpass.merchant.TransactionResultListener;

import java.util.Currency;

/***
 * Activity used at checkout.
 */
public class CheckoutActivity extends BaseActivity
        implements BasketAdapter.BasketListener {

    /**
     * TAG used for logging.
     */
    private static final String TAG = CheckoutActivity.class.getName();

    /**
     * Delay for "Completing payment" dialog
     */
    private static final int DIALOG_DELAY_MILLIS = 1500;

    /**
     * The current basket.
     */
    private final Basket mBasket = DataManager.getInstance().mBasket;

    /**
     * UI Elements.
     */
    private BasketAdapter mBasketAdapter;
    private TextView mTxtSubtotal;

    /**
     * Data Manager.
     */
    private final DataManager mDataManager = DataManager.getInstance();

    /**
     * The "Buy with MasterPass" button.
     */
    private RelativeLayout mLayoutBuyWithMasterPass;

    /**
     * Defines if the progress dialog should close after a delay, or if it should close
     * immediately when returning from MCO.
     */
    private boolean mDelayCloseProgressDialog = false;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Proceed to the completion activity
            Intent newIntent = new Intent(CheckoutActivity.this, CompleteActivity.class);
            newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(newIntent);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        // Configure the Action Bar
        configureActionBar(true);
        updateBadge(DataManager.getInstance().mBasket.mTotalItems);

        mLayoutBuyWithMasterPass = (RelativeLayout) findViewById(R.id.layout_buy_with_masterpass);

        // Ensure no shipping is currently applied
        mBasket.mShippingPrice = 0;

        // Ensure the scroll view is scrolled right to the top
        final ScrollView mScrollView = (ScrollView) findViewById(R.id.scrollview);
        mScrollView.post(new Runnable() {
            @Override
            public void run() {
                mScrollView.scrollTo(0, 0);
            }
        });

        // Setup the list of products
        mBasketAdapter = new BasketAdapter(this, mBasket, this);
        ListView mListProducts = (ListView) findViewById(android.R.id.list);
        mListProducts.setAdapter(mBasketAdapter);
        mListProducts.setItemsCanFocus(true);
//        mListProducts.setOnItemClickListener(this);

        // Update the total and subtotal
        mTxtSubtotal = (TextView) findViewById(R.id.txt_subtotal);
        updateBasketTotals();

        // Initializes the [MCO-SDK] checkout
        initMCOCheckout();

        // Setup the close basket icon
        TextView txtCloseIcon = (TextView) findViewById(R.id.txt_basket_close_icon);
        txtCloseIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backToProducts();
            }
        });

        //Button checkoutButton = (Button) findViewById(R.id.btn_checkout);
        ImageButton checkoutButton = (ImageButton) findViewById(R.id.btn_checkout);
        checkoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CheckoutActivity.this, CheckoutFormActivity.class));
            }
        });

        registerReceiver(broadcastReceiver, new IntentFilter("COMPLETE_CHECKOUT"));
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateBadge(DataManager.getInstance().mBasket.mTotalItems);
        if (mDelayCloseProgressDialog) {

            mDelayCloseProgressDialog = false;

            Log.v(TAG, "Delayed close dialog");

            // After a short delay, close the dialog that was opened in transactionInitiated()
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {

                    // Close last instantiated progress dialog
                    DialogHelper.closeProgressDialog();
                }
            }, DIALOG_DELAY_MILLIS);

        } else {
            // In case user clicked 'Back' and returned to this page
            Log.v(TAG, "Immediate close dialog");

            // Close last instantiated progress dialog
            DialogHelper.closeProgressDialog();

        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(broadcastReceiver);
    }

    /***
     * [MCO-SDK] Initializes everything needed to checkout using MCO.
     */
    private void initMCOCheckout() {
        Log.v(TAG, "initMCOCheckout");

        // Check if the MCO SDK has been initialized. Initialization is started in StartActivity
        if (mDataManager.isMcoInitialized()) {

            RelativeLayout.LayoutParams btnParams = new
                    RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT);
            btnParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            btnParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

            try {
                ShippingAddressesManager.INSTANCE.setAddressUsedForTransaction(null);
                final TransactionDetails transactionDetails = new TransactionDetails();
                transactionDetails.setIsShippingRequired(mDataManager.mBasket.doesBasketRequireShipping());
                long total = Long.parseLong(mDataManager.mBasket.getSubTotalToString());
                double discount = total * 0.05;
                mDataManager.mBasket.setDiscount(discount/100);
                total -= (long)discount; //5% off
                transactionDetails.setAmountData(
                        new AmountData(
                                total, //Long.parseLong(mDataManager.mBasket.getSubTotalToString()),
                                0l,
                                total, //Long.parseLong(mDataManager.mBasket.getSubTotalToString()),
                                Currency.getInstance(mDataManager.mBasket.mCurrencyCode)));

                /*// Set up a listener so the MCO SDK can retrieve the transactionDetails on request
                MasterPassButton transactionDetailsListener = new MasterPassButtonListener() {
                    @Override
                    public TransactionDetails getTransactionDetails() {
                        return transactionDetails;
                    }
                };*/

                // Get MasterPass Button from MCO SDK
                MasterPassButton mBuyWithMasterPassButton =
                        MasterPass.getInstance()
                                .getMasterPassButton(
                                        transactionDetails,
                                        new PaymentConfirmationIntegration(),
                                        mTransactionResultListener);

                if (mBuyWithMasterPassButton != null) {
                    Log.d(TAG, "getMasterPassButton: received");

                    // Show the MasterPass button
                    mBuyWithMasterPassButton.setLayoutParams(btnParams);
                    mLayoutBuyWithMasterPass.removeAllViews();
                    mBuyWithMasterPassButton.setForeground( getResources().getDrawable(R.drawable.buy_with_masterpas_pod_dark));
                    mLayoutBuyWithMasterPass.addView(mBuyWithMasterPassButton);
                } else {
                    Log.e(TAG, "getMasterPassButton: null");
                }

            } catch (MasterPassException e) {
                Log.e(TAG, "Getting MasterPass button exception: " + e.getMessage() + " " +
                        e.getErrorMessage() + " " + e.getErrorCode());
            }

        } else {
            // MCO not ready so try in a moment
            Log.d(TAG, "MCO not yet initialized, re-schedule in 1s");
            (new Handler()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    CheckoutActivity.this.initMCOCheckout();
                }
            }, 1000);// 1s
        }

        Log.d(TAG, "Init MCO Checkout method end");
    }

    /**
     * [MCO-SDK] Transaction result listener passed to MCO.
     * Defines behaviour to occur at different states within the transaction.
     */
    private final TransactionResultListener mTransactionResultListener = new TransactionResultListener() {

        /**
         * Raised when the transaction has been cancelled.
         */
        @Override
        public void transactionCanceled() {
            Log.d(TAG, "TransactionResultListener: transactionCanceled");
            // If the transaction is cancelled, then make sure the progress dialog stays on screen
            // for a short time.
            mDelayCloseProgressDialog = true;

            // After a short delay, close the dialog that was opened in transactionInitiated()
            // and show the error message. Delay is needed for user to see message.
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    DialogHelper.showDialog(CheckoutActivity.this, "Transaction Cancelled", "Could not complete the transaction", null);

                }
            }, DIALOG_DELAY_MILLIS);
        }

        /**
         * Raised when the transaction fails.
         * @param e Exception object raised by the MCO SDK.
         */
        @Override
        public void transactionFailed(final MasterPassException e) {

                Log.d(TAG, "TransactionResultListener: transactionFailed " + e.toString() + " " + e.getErrorCode() + " " + e.getErrorMessage() + " " + e.getMessage());
                e.printStackTrace();

                // If the transaction failed, then make sure the progress dialog stays on screen
                // for a short time.
                mDelayCloseProgressDialog = true;

                // After a short delay, close the dialog that was opened in transactionInitiated()
                // and show the error message. Delay is needed for user to see message.
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        DialogHelper.showDialog(CheckoutActivity.this, "Transaction Failed", "Could not complete the transaction.\n" +
                                e.getMessage(), null);

                    }
                }, DIALOG_DELAY_MILLIS);


                // If the transaction failed, then make sure the progress dialog stays on screen
                // for a short time.



        }

        /**
         * Raised when the transactions details are incorrect.
         * @param e Exception object raised by the MCO SDK.
         */
        @Override
        public void transactionDetailsInputException(final MasterPassException e) {
            Log.d(TAG, "TransactionResultListener: transactionDetailsInputException " + e.getErrorCode() + " " + e.getErrorMessage() + " " + e.getMessage() + " " + e.getCause());
            e.printStackTrace();

            // If the transaction details error occur, then make sure the progress dialog stays on screen
            // for a short time.
            mDelayCloseProgressDialog = true;

            // After a short delay, close the dialog that was opened in transactionInitiated()
            // and show the error message. Delay is needed for user to see message.
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {

                    DialogHelper.showDialog(CheckoutActivity.this, "Transaction Details Error", "Could not complete the transaction.\n" +
                            e.getMessage(), null);

                }
            }, DIALOG_DELAY_MILLIS);
        }

        /**
         * Raised when the transaction has been initiated.
         */
        @Override
        public void transactionInitiated() {
            Log.d(TAG, "TransactionResultListener: transactionInitiated");

            // Show a progress dialog which will become visible when the MCO activity closes itself
            DialogHelper.showProgressDialog(CheckoutActivity.this, "Completing Payment");
        }

        /**
         * Raised when the transaction has been authorized/completed.
         */
        @Override
        public void transactionAuthorized(AuthorizationResponse authorizationResponse) {
            Log.d(TAG, "TransactionResultListener: transactionAuthorized");

            // If the transaction failed, then make sure the progress dialog stays on screen
            // for a short time.
            mDelayCloseProgressDialog = true;

            // Save address used for transaction
            ShippingAddressesManager.INSTANCE.saveAddressUsedForTransaction();
            ShippingAddressesManager.INSTANCE.setAddressUsedForTransaction(null);
        }
    };

    /**
     * Update the subtotal and total based on the current basket.
     */
    private void updateBasketTotals() {
        mTxtSubtotal.setText(StringHelper.asCurrency(mBasket.mCurrencyCode, mBasket.mSubTotal));
    }

    /**
     * Override the onBackPressed behaviour to force navigation to the products page.
     */
    @Override
    public void onBackPressed() {
        backToProducts();
    }

    /**
     * Navigate to the products page whilst clearing the backstack.
     */
    private void backToProducts() {
        Intent intent = new Intent(this, ProductsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
    }

    @Override
    public void incrementQuantity(BasketItem item) {
        // Add it to the basket
        mBasket.addProduct(item.mProduct);
        updateBadge(DataManager.getInstance().mBasket.mTotalItems);

        // Update out on-screen totals
        updateBasketTotals();

        // Because the Basket is shared by the adapter, it will have automatically updated
        mBasketAdapter.notifyDataSetChanged();
        updateBadge(DataManager.getInstance().mBasket.mTotalItems);

        // Reinitialize MCO-SDK to update amount that will be passed further
        initMCOCheckout();
    }

    @Override
    public void decrementQuantity(BasketItem item) {
        // Remove a single count from the basket
        int numItemsLeft = mBasket.removeProduct(item.mProduct);


        // Update out on-screen totals
        updateBasketTotals();

        // If there are no items left in the basket, then go back to the products activity
        if (numItemsLeft <= 0) {
            Log.w(TAG, "No items left in basket");
            DialogHelper.emptyBasket(this, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    backToProducts();
                }
            });
        }

        // Because the Basket is shared by the adapter, it will have automatically updated
        mBasketAdapter.notifyDataSetChanged();
        updateBadge(DataManager.getInstance().mBasket.mTotalItems);

        // Reinitialize MCO-SDK to update amount that will be passed further
        initMCOCheckout();
    }

    @Override
    public void removeItem(BasketItem item) {
        // Remove from the basket
        int numItemsLeft = mBasket.removeAllOfProduct(item.mProduct);

        // Update out on-screen totals
        updateBasketTotals();

        // If there are no items left in the basket, then go back to the products activity
        if (numItemsLeft <= 0) {
            Log.w(TAG, "No items left in basket");
            DialogHelper.emptyBasket(this, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    backToProducts();
                }
            });
        }

        // Because the Basket is shared by the adapter, it will have automatically updated
        mBasketAdapter.notifyDataSetChanged();
        updateBadge(DataManager.getInstance().mBasket.mTotalItems);

        // Reinitialize MCO-SDK to update amount that will be passed further
        initMCOCheckout();
    }
}