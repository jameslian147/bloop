/*
 *  ****************************************************************************
 *  Copyright (c) 2015, MasterCard International Incorporated and/or its
 *  affiliates. All rights reserved.
 *  <p/>
 *  The contents of this file may only be used subject to the MasterCard
 *  Mobile Payment SDK for MCBP and/or MasterCard Mobile MPP UI SDK
 *  Materials License.
 *  <p/>
 *  Please refer to the file LICENSE.TXT for full details.
 *  <p/>
 *  TO THE EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 *  WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NON INFRINGEMENT. TO THE EXTENT PERMITTED BY LAW, IN NO EVENT SHALL
 *  MASTERCARD OR ITS AFFILIATES BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 *  *****************************************************************************
 */

package com.mastercard.foreverythingelse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.mastercard.foreverythingelse.Constants;
import com.mastercard.foreverythingelse.R;
import com.mastercard.foreverythingelse.helpers.StringHelper;
import com.mastercard.foreverythingelse.model.Product;

import java.util.ArrayList;

/**
 * Adapter for products list used on ProductsActivity
 */
public class ProductListAdapter extends ArrayAdapter<Product> {

    public interface AddToCartListener {
        void addToCart(Product product, View parentView, ImageView productImageView);
    }

    private AddToCartListener mListener;

    public ProductListAdapter(Context context, ArrayList<Product> products) {
        super(context, R.layout.list_item_product, products);
        mListener = null;
    }

    public ProductListAdapter(Context context, ArrayList<Product> products, AddToCartListener listener) {
        super(context, R.layout.list_item_product, products);
        mListener = listener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ProductHolder holder;

        // If we've not been passed a view (i.e. not recycling) we need to inflate one
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_product, null);

            // Create a new holder and tag it against the view
            holder = new ProductHolder(convertView, mListener);
            convertView.setTag(holder);
        } else {
            // Grab the current tag
            holder = (ProductHolder) convertView.getTag();
        }

        // Populate the view with the data from the product in the current position
        holder.populate(getItem(position));

        // Return the populate view
        return convertView;
    }

    /**
     * View Holder pattern
     */
    private static class ProductHolder {
        private final View mParent;
        private final ImageView mImage;
        private final TextView mName;
        private final TextView mTxtPrice;
        private final ImageButton mBtnAddToCart;
        private final RatingBar mRatigBar;
        //private final ImageView mSaleImage;
        private AddToCartListener mListener = null;

        private ProductHolder(View container) {
            mParent = container;
            mImage = (ImageView) container.findViewById(R.id.img_image);
            mName = (TextView) container.findViewById(R.id.txt_prd_name);
            mTxtPrice = (TextView) container.findViewById(R.id.txt_price);
            mRatigBar = (RatingBar) container.findViewById(R.id.ratingBar);
            mBtnAddToCart = (ImageButton) container.findViewById(R.id.btn_add_to_cart);
            mBtnAddToCart.setClickable(false);
            mBtnAddToCart.setFocusable(false);
            //mSaleImage = (ImageView) container.findViewById(R.id.img_on_sale);

            mTxtPrice.setVisibility(View.INVISIBLE);
            mName.setVisibility(View.INVISIBLE);
            mRatigBar.setVisibility(View.INVISIBLE);
            mBtnAddToCart.setVisibility(View.INVISIBLE);
        }

        private ProductHolder(View container, AddToCartListener listener) {
            this(container);
            mListener = listener;

            if (mListener != null) {
                mName.setVisibility(View.VISIBLE);
                mTxtPrice.setVisibility(View.VISIBLE);
                mRatigBar.setVisibility(View.VISIBLE);
                mBtnAddToCart.setVisibility(View.VISIBLE);
            }
        }

        private void populate(final Product product) {
            mImage.setImageResource(product.mImageResource);
            mName.setText(product.mName);
            mTxtPrice.setText(StringHelper.asCurrency(Constants.CURRENCY_CODE, product.mPrice));
            mRatigBar.setRating(product.mRating);
            mBtnAddToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.addToCart(product, mParent, mImage);
                    }
                }
            });

           /* if (product.mIsOnSalse) {
                mSaleImage.setVisibility(View.VISIBLE);
            } else {
                mSaleImage.setVisibility(View.INVISIBLE);
            }*/
        }
    }
}
