/*
 *  ****************************************************************************
 *  Copyright (c) 2015, MasterCard International Incorporated and/or its
 *  affiliates. All rights reserved.
 *  <p/>
 *  The contents of this file may only be used subject to the MasterCard
 *  Mobile Payment SDK for MCBP and/or MasterCard Mobile MPP UI SDK
 *  Materials License.
 *  <p/>
 *  Please refer to the file LICENSE.TXT for full details.
 *  <p/>
 *  TO THE EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 *  WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NON INFRINGEMENT. TO THE EXTENT PERMITTED BY LAW, IN NO EVENT SHALL
 *  MASTERCARD OR ITS AFFILIATES BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 *  *****************************************************************************
 */

package com.mastercard.foreverythingelse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mastercard.foreverythingelse.Constants;
import com.mastercard.foreverythingelse.R;
import com.mastercard.foreverythingelse.helpers.StringHelper;
import com.mastercard.foreverythingelse.model.Basket;
import com.mastercard.foreverythingelse.model.BasketItem;

/**
 * Adapter for list of products in current basket.
 * Used on CheckoutActivity and on CompleteActivity
 */
public class BasketAdapter extends ArrayAdapter<BasketItem> {

    public interface BasketListener {
        void incrementQuantity(BasketItem item);

        void decrementQuantity(BasketItem item);

        void removeItem(BasketItem item);
    }

    /**
     * The class listening to basket events
     */
    private BasketListener mListener;

    /**
     * Ctor
     *
     * @param context current context
     * @param basket  current basket
     */
    public BasketAdapter(Context context, Basket basket, BasketListener listener) {
        super(context, R.layout.list_item_basket_item, basket.mBasketItems);
        mListener = listener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BasketItemHolder holder;

        // If we've not been passed a view (i.e. not recycling) we need to inflate one
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_basket_item, null);

            // Create a new holder and tag it against the view
            holder = new BasketItemHolder(convertView);
            convertView.setTag(holder);

        } else {

            // Grab the current tag
            holder = (BasketItemHolder) convertView.getTag();
        }

        // Populate the view with the data from the product in the current position
        holder.populate(getItem(position));

        // Return the populate view
        return convertView;
    }

    /**
     * Holder class to the re-use of views.
     */
    private class BasketItemHolder {
        // Widgets
        private final ImageView mImage;
        private final TextView mTxtTitle;
        private final TextView mTxtPrice;
        private final TextView mTxtQuantity;
        private final TextView mTxtRemove;
        private final Button mBtnDecrement;
        private final Button mBtnIncrement;

        private BasketItemHolder(View container) {
            mImage = (ImageView) container.findViewById(R.id.img_image);
            mTxtTitle = (TextView) container.findViewById(R.id.txt_name);
            mTxtPrice = (TextView) container.findViewById(R.id.txt_price);
            mTxtQuantity = (TextView) container.findViewById(R.id.txt_quantity);
            mTxtRemove = (TextView) container.findViewById(R.id.txt_remove);
            mBtnDecrement = (Button) container.findViewById(R.id.btn_decrement);
            mBtnIncrement = (Button) container.findViewById(R.id.btn_increment);
        }

        private void populate(final BasketItem item) {
            mImage.setImageResource(item.mProduct.mImageResource);
            mTxtTitle.setText(item.mProduct.mName);
            mTxtPrice.setText(StringHelper.asCurrency(Constants.CURRENCY_CODE, item.mProduct.mPrice * item.mQuantity));
            mTxtQuantity.setText(String.valueOf(item.mQuantity));

            // Bind click events
            mTxtRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.removeItem(item);
                }
            });

            mBtnDecrement.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.decrementQuantity(item);
                }
            });

            mBtnIncrement.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.incrementQuantity(item);
                }
            });
        }
    }
}
