package com.mastercard.foreverythingelse.widget;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.mastercard.foreverythingelse.R;

import java.util.Hashtable;

public class FontButton extends Button {
    private static Hashtable<String, Typeface> fonts = new Hashtable<>();

    public FontButton(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);

        if (!isInEditMode()) {
            final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.FontTextView);
            final String font = a.getString(R.styleable.FontTextView_fontname);
            setTypeface(getFont(font));
        }
    }

    public FontButton(final Context context, final AttributeSet attrs) {
        super(context, attrs);

        if (!isInEditMode()) {
            Typeface defaultFont;
            final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.FontTextView);
            final String font = a.getString(R.styleable.FontTextView_fontname);
            if (font != null) {
                defaultFont = getFont(font);
            } else {
                defaultFont = getFont(context.getResources().getString(R.string.font));
            }

            setTypeface(defaultFont);
        }
    }

    public FontButton(final Context context) {
        super(context);

        if (!isInEditMode()) {
            setTypeface(getFont(context.getResources().getString(R.string.font)));
        }
    }

    private Typeface getFont(final String fontName) {
        Typeface type = fonts.get(fontName);
        if (type == null) {
            type = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + fontName);
            fonts.put(fontName, type);
        }
        return type;
    }
}
