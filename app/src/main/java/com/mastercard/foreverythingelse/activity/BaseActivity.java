/*
 *  ****************************************************************************
 *  Copyright (c) 2015, MasterCard International Incorporated and/or its
 *  affiliates. All rights reserved.
 *  <p/>
 *  The contents of this file may only be used subject to the MasterCard
 *  Mobile Payment SDK for MCBP and/or MasterCard Mobile MPP UI SDK
 *  Materials License.
 *  <p/>
 *  Please refer to the file LICENSE.TXT for full details.
 *  <p/>
 *  TO THE EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 *  WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NON INFRINGEMENT. TO THE EXTENT PERMITTED BY LAW, IN NO EVENT SHALL
 *  MASTERCARD OR ITS AFFILIATES BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 *  *****************************************************************************
 */

package com.mastercard.foreverythingelse.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.mastercard.foreverythingelse.DataManager;
import com.mastercard.foreverythingelse.R;
import com.mastercard.foreverythingelse.helpers.DialogHelper;
import com.mastercard.foreverythingelse.model.Product;

/**
 * Simple base Activity that has a method to setup the Action Bar throughout the application.
 */
public abstract class BaseActivity extends AppCompatActivity {
    private static final String TAG = BaseActivity.class.getName();

    /**
     * Animation/position helpers.
     */
    private int mContentOffset = 0;
    private int mCheckoutX = 0;
    private int mCheckoutY = 0;

    protected FrameLayout mFrameContainer;

    /**
     * TextView for the number of products currently in the basket.
     */
    private TextView mTxtProductBadge;

    protected void configureActionBar() {
        configureActionBar(false);
    }

    /**
     * Setup the Action Bar.
     */
    protected void configureActionBar(boolean showMenu) {
        final ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setCustomView(R.layout.action_bar);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayUseLogoEnabled(false);
            actionBar.setDisplayShowHomeEnabled(false);


            // Pull out the badge view
            View actionBarView = actionBar.getCustomView();
            mTxtProductBadge = (TextView) actionBarView.findViewById(R.id.txt_product_count);
            final ImageView manageAddresses = (ImageView) actionBarView.findViewById(R.id.notification);
            final ImageView basket = (ImageView) actionBarView.findViewById(R.id.img_checkout);

            if (showMenu) {
                View.OnClickListener basketClickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Check the basket actually has some items in it
                        if (DataManager.getInstance().mBasket.mTotalItems > 0) {
                            // Go to the checkout activity
                            Intent intent = new Intent(BaseActivity.this, CheckoutActivity.class);
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
                        } else {
                            DialogHelper.emptyBasket(BaseActivity.this);
                        }
                    }
                };
                mTxtProductBadge.setOnClickListener(basketClickListener);
                basket.setOnClickListener(basketClickListener);

                manageAddresses.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(BaseActivity.this, ManageShippingAddressesActivity.class));
                    }
                });
            } else {
                mTxtProductBadge.setVisibility(View.INVISIBLE);
                manageAddresses.setVisibility(View.INVISIBLE);
                basket.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        //menuInflater.inflate(R.menu.main_menu,menu);
        return true;
    }

    /**
     * Update the badge for number of products in basket.
     *
     * @param newCount New number to show in the badge.
     */
    protected void updateBadge(int newCount) {
        // Make sure we have the reference to the badge
        if (mTxtProductBadge != null) {
            mTxtProductBadge.setText(String.valueOf(newCount));
            mTxtProductBadge.setVisibility(View.VISIBLE);
        } else {
            Log.w(TAG, "Trying to set badge value without reference to the badge");
        }
    }

    protected void goToProductDetail(int position) {
        Intent intent = new Intent(this, ProductDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(ProductDetailActivity.INTENT_EXTRA_PRODUCT_INDEX, position);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void calculateOffsetsIfNecessary() {
        // Get the offset position of the content if we don't have it already
        // (essentially this is the action bar)
        if (mContentOffset == 0) {
            mContentOffset = getWindow().findViewById(Window.ID_ANDROID_CONTENT).getTop();
        }

        // Get the position of the badge if we don't have it already
        if (mCheckoutX == 0 && mCheckoutY == 0) {
            int[] pos = new int[2];
            mTxtProductBadge.getLocationOnScreen(pos);
            mCheckoutX = pos[0];
            mCheckoutY = pos[1] - mContentOffset;
        }
    }

    protected void animateAddToCart(Product product, ImageView productImageView) {
        calculateOffsetsIfNecessary();

        // We need the dimensions and position on the screen of this image
        int width = productImageView.getWidth();
        int height = productImageView.getHeight();
        int[] pos = new int[2];
        productImageView.getLocationInWindow(pos);
        int x = pos[0];
        int y = pos[1] - mContentOffset;

        // Create a "clone" of the product image the same size and in the same position
        final ImageView clone = new ImageView(this);
        clone.setImageResource(product.mImageResource);
        clone.setAlpha(0.8f);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(width, height);
        params.leftMargin = x;
        params.topMargin = y;
        clone.setLayoutParams(params);

        // Add the image to our container
        mFrameContainer.addView(clone);

        // Build the translate animation
        TranslateAnimation translate = new TranslateAnimation(0, mCheckoutX - x, 0, mCheckoutY - y);
        translate.setDuration(1000);

        // Build the scale animation
        ScaleAnimation scale = new ScaleAnimation(1.0f, 0.33f, 1.0f, 0.33f);
        scale.setDuration(1000);

        // Build the fade animation
        AlphaAnimation alpha = new AlphaAnimation(0.8f, 0.0f);
        alpha.setDuration(800);

        // Combine the animations into a set of animations
        AnimationSet set = new AnimationSet(true);
        set.addAnimation(scale);
        set.addAnimation(translate);
        set.addAnimation(alpha);
        set.setFillAfter(true);

        set.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                // Intentional no-op
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // Ensure the image view is removed after the animation is complete
                mFrameContainer.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mFrameContainer.removeView(clone);
                    }
                }, 1000);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // Intentional no-op
            }
        });

        // Run the animation
        clone.startAnimation(set);
    }
}
