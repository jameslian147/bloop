/*
 *  ****************************************************************************
 *  Copyright (c) 2015, MasterCard International Incorporated and/or its
 *  affiliates. All rights reserved.
 *  <p/>
 *  The contents of this file may only be used subject to the MasterCard
 *  Mobile Payment SDK for MCBP and/or MasterCard Mobile MPP UI SDK
 *  Materials License.
 *  <p/>
 *  Please refer to the file LICENSE.TXT for full details.
 *  <p/>
 *  TO THE EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 *  WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NON INFRINGEMENT. TO THE EXTENT PERMITTED BY LAW, IN NO EVENT SHALL
 *  MASTERCARD OR ITS AFFILIATES BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 *  *****************************************************************************
 */

package com.mastercard.foreverythingelse.mco;

import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.VolleyError;
import com.mastercard.foreverythingelse.Constants;
import com.mastercard.foreverythingelse.DataManager;
import com.mastercard.foreverythingelse.manager.ShippingAddressesManager;
import com.mastercard.foreverythingelse.manager.ShippingMethodsManager;
import com.mastercard.foreverythingelse.model.DbAddressModel;
import com.mastercard.foreverythingelse.network.ResponseListener;
import com.mastercard.foreverythingelse.network.RestMethod;
import com.mastercard.foreverythingelse.network.api.ApiList;
import com.mastercard.foreverythingelse.network.api.model.DSRP;
import com.mastercard.foreverythingelse.network.api.model.DSRPOptions;
import com.mastercard.foreverythingelse.network.api.model.ExtensionPointMerchantInitialization;
import com.mastercard.foreverythingelse.network.api.model.Option;
import com.mastercard.foreverythingelse.network.api.request.MerchantInitializationRequest;
import com.mastercard.foreverythingelse.network.api.response.MerchantInitializationResponse;
import com.mastercard.foreverythingelse.network.api.response.RequestTokenResponse;
import com.mastercard.foreverythingelse.util.PreferencesHelper;
import com.mastercard.masterpass.core.CryptogramType;
import com.mastercard.masterpass.core.MasterPassAddress;
import com.mastercard.masterpass.core.MasterPassException;
import com.mastercard.masterpass.merchant.AmountData;
import com.mastercard.masterpass.merchant.CheckoutInitiationCallback;
import com.mastercard.masterpass.merchant.PaymentConfirmationCallback;
import com.mastercard.masterpass.merchant.ShippingMethod;

import java.util.List;
import java.util.Random;

/**
 * [MCO-SDK] Callback class passed to MCO when retrieving the MasterPass button.
 * It is responsible for interacting with user when he is on Payment Confirmation page.
 * Used to provide shipping addresses list, shipping methods list, default values
 * and confirming total amount after shipping costs.
 */
public class PaymentConfirmationIntegration implements PaymentConfirmationCallback {

    /**
     * TAG for logging purposes.
     */
    private static final String TAG = PaymentConfirmationIntegration.class.getName();

    /***
     * Constructor.
     */
    public PaymentConfirmationIntegration() {
    }

    /***
     * Retrieves a list of shipping methods valid for the supplied address.
     *
     * @param masterPassAddress The address with which we want to find shipping methods for.
     * @return List of valid shipping methods.
     */
    @Override
    public List<ShippingMethod> getUpdatedShippingMethodsList(MasterPassAddress masterPassAddress) {
        Log.v(TAG, "getUpdatedShippingMethodsList");
        // We are using static list of shipping methods not taking into account chosen address
        return ShippingMethodsManager.INSTANCE.getSupportedShippingMethods();
    }

    /***
     * Retrieves address list from merchant application.
     *
     * @return List of shipping addresses.
     */
    @Override
    public List<MasterPassAddress> getShippingAddressesList(boolean b) {
        Log.v(TAG, "getShippingAddressesList");
        List<DbAddressModel> dbAddressModels = ShippingAddressesManager.INSTANCE.getAddresses();
        return ShippingAddressesManager.INSTANCE.dbAddressListToMasterPassAddressList(dbAddressModels);
    }

    /***
     * Retrieves default shipping method ID from merchant application.
     *
     * @return Default shipping method ID.
     */
    @Override
    public String getDefaultShippingMethodId() {
        Log.v(TAG, "getDefaultShippingMethodId");
        Log.d(TAG, "Default shipping method id: " + ShippingMethodsManager.INSTANCE.getDefaultShippingMethodId());
        return String.valueOf(ShippingMethodsManager.INSTANCE.getDefaultShippingMethodId());
    }

    /***
     * Gets the default shipping address ID.
     *
     * @return Default shipping address ID.
     */
    @Override
    public String getDefaultShippingAddressId() {
        Log.v(TAG, "getDefaultShippingAddressId");
        MasterPassAddress defaultShippingAddress = ShippingAddressesManager.INSTANCE.getDefaultShippingAddress();
        if (defaultShippingAddress != null) {
            Log.d(TAG, "Default shipping address id: " + defaultShippingAddress.getId());
            return defaultShippingAddress.getId();
        } else {
            Log.d(TAG, "Default shipping address id: null");
            return null;
        }
    }

    /***
     * Requests checkout initiation. Mocks contact with server to authorize checkout.
     *
     * @param checkoutInitiationCallback Callback class that will be triggered from this method.
     */
    @Override
    public void checkoutInitiationRequest(final CheckoutInitiationCallback checkoutInitiationCallback) {
        Log.v(TAG, "checkoutInitiationRequest");

        // Make a call to Merchant Server
        DataManager.getInstance().getNetworkManager().makeApiCall(RestMethod.POST,
                ApiList.Method.API_REQUEST_TOKEN, null, new ResponseListener<RequestTokenResponse>() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                        checkoutInitiationCallback.onFailure();
                    }

                    @Override
                    public void onResponse(final RequestTokenResponse requestTokenResponse) {
                        // Prepare request object
                        MerchantInitializationRequest merchantInitRequest = new MerchantInitializationRequest();
                        merchantInitRequest.setRequestToken(requestTokenResponse.requestToken);
                        merchantInitRequest.setOriginUrl(Constants.DEFAULT_ORIGIN_URL);
                        // Prepare DSRP Data
                        CryptogramType cryptogramTypeEnum = CryptogramType.UCAF;
                        Option option = null;
                        if (cryptogramTypeEnum == CryptogramType.UCAF) {
                            option = new Option(Constants.DEFAULT_BRAND_ID, "UCAF");
                        } else {
                            // In case ICC or UCAF_AND_ICC use ICC as it is stronger
                            option = new Option(Constants.DEFAULT_BRAND_ID, "ICC");
                        }
                        Log.d(TAG, "Requesting cryptogram: " + option.getAcceptanceType());
                        DSRPOptions dsrpOptions = new DSRPOptions(option);
                        // Set DSRP object
                        DSRP dsrp = new DSRP(dsrpOptions, null);// Empty UN, so Switch will generate one
                        merchantInitRequest.setExtensionPoint(new ExtensionPointMerchantInitialization(dsrp));
                        // Make a call to Merchant Server
                        DataManager.getInstance().getNetworkManager().makeApiCall(RestMethod.POST,
                                ApiList.Method.API_MERCHANT_INITIALIZATION,
                                merchantInitRequest,
                                new ResponseListener<MerchantInitializationResponse>() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Log.d(TAG, error.toString());
                                        checkoutInitiationCallback.onFailure();
                                    }

                                    @Override
                                    public void onResponse(MerchantInitializationResponse merchantInitializationResponse) {

                                        if (merchantInitializationResponse == null) {
                                            Log.d(TAG, "MerchantInitialization response is null");
                                            checkoutInitiationCallback.onFailure();
                                            return;
                                        }

                                        // Check if requestToken is the same that we passed in request
                                        if (!merchantInitializationResponse.requestToken.equalsIgnoreCase(requestTokenResponse.requestToken)) {
                                            Log.d(TAG, "Token returned in MerchantInitialization is different then requested");
                                            checkoutInitiationCallback.onFailure();
                                            return;
                                        }
                                        if (merchantInitializationResponse.extensionPoint != null
                                                && merchantInitializationResponse.extensionPoint.getUnpredictableNumber() != null
                                                && !merchantInitializationResponse.extensionPoint.getUnpredictableNumber().isEmpty()) {
                                            checkoutInitiationCallback.onSuccess(requestTokenResponse.requestToken, merchantInitializationResponse.extensionPoint.getUnpredictableNumber());
                                        } else {
                                            Log.e(TAG, "Switch didn't return Unpredictable Number. Cannot proceed");
                                            checkoutInitiationCallback.onFailure();
                                        }
                                    }
                                });

                    }
                });



    }

    /***
     * Gets the full amount data, including the cost of shipping using the supplied shipping method.
     *
     * @param masterPassAddress Shipping address.
     * @param selectedMethodId  Shipping method.
     * @param amountData        Existing amount data.
     * @return Updated amount data.
     * @throws MasterPassException
     */
    @Override
    public AmountData getFullAmountData(
            MasterPassAddress masterPassAddress,
            @Nullable String selectedMethodId,
            AmountData amountData)
            throws MasterPassException {
        Log.v(TAG, "getFullAmountData");

        if (selectedMethodId == null || masterPassAddress == null) {
            // If shipping method has not been supplied, then we just return the amount data without
            // calculating extra shipping cost.
            return new AmountData(amountData.getEstimatedTotal(), amountData.getTax(), amountData.getEstimatedTotal(), amountData.getCurrency());
        }
        // Save address chosen for transaction
        ShippingAddressesManager.INSTANCE.setAddressUsedForTransaction(masterPassAddress);

        // Find selected shipping method
        ShippingMethod selectedShippingMethod = ShippingMethodsManager.INSTANCE.getShippingMethodById(selectedMethodId);

        // Check that a shipping method exists for the supplied ID
        if (selectedShippingMethod != null) {

            // Get existing sub-total
            long newTotal = amountData.getEstimatedTotal();

            // Add shipping cost of the selected shipping method
            newTotal += Float.parseFloat(selectedShippingMethod.getShippingCost());

            // Store the shipping cost
            DataManager.getInstance().mBasket.mShippingPrice = Double.valueOf(selectedShippingMethod.getShippingCost())/100;

            // Return the newly calculated amount data model
            return new AmountData(amountData.getEstimatedTotal(), amountData.getTax(), newTotal, amountData.getCurrency());

        } else {

            // Can't find the selected method - throw exception
            throw new MasterPassException("custom-code", "Can't find given shipping method with id=" + selectedMethodId);
        }
    }
}
